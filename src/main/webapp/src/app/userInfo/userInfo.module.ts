import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { UserInfoRoutingModule } from './userInfo-routing.module';
import { UserInfoComponent } from './userInfo.component';
import { APP_BASE_HREF } from '@angular/common';


@NgModule({
  declarations: [
    UserInfoComponent
  ],
  imports: [
    BrowserModule,
    UserInfoRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [{provide: APP_BASE_HREF, useValue: '/'}],
  bootstrap: [UserInfoComponent]
})
export class UserInfoModule { 
 
}
