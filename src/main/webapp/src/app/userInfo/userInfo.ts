export class UserInfo{
    id: string;
    name: string;
    character: string;
    age: Int16Array;
    introduction: string;
}