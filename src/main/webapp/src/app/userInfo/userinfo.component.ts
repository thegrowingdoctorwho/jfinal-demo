import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpClientModule } from '@angular/common/http';
import { UserInfo } from './userInfo';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  })
};



@Component({
  selector: 'app-userInfo',
  templateUrl: './userInfo.html',
  styleUrls: ['./userInfo.component.css']
})



export class UserInfoComponent implements OnInit {
  
  

  ngOnInit(): void {
    //bootstrap模态框处理。
    

  }
  
  constructor(private http:HttpClient){}
  title = '用户展示';
  userAddIdex = 10;
  userDelIdex = 30;
  userUpdateIdex = 50;
  userSearchIdex = 70;

  //测试用数据
  userInfo = {"id":23,
              "name":"jerry23",
              "character":"test1",
              "age":18,
              "introduction":"very good"
  }

  //提交方法
  userAdd() {
    // const body = JSON.parse;
   
    this.http.post("http://127.0.0.1:8080/users/add",this.userInfo, httpOptions).subscribe(response=>{
      console.log;
    });
            
  }

  //删除方法
  userDel() {
    this.http.delete("http://127.0.0.1:8080/users/add",).subscribe(response=>{
      console.log;
    });
  }

  //更新方法
  userUpdate() {
    this.http.put("http://127.0.0.1:8080/users/add",this.userInfo).subscribe(response=>{
      console.log;
    });
  }

  //查询方法
  userSearch() {
    this.http.get("http://127.0.0.1:8080/users/add",).subscribe(response=>{
      console.log;
    });
  }
 

}
