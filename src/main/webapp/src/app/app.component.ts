import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

@Component({
  selector: 'app-user',
  templateUrl: './userInfo/userInfo.component.html',
  styleUrls: ['./userInfo/userInfo.component.css']
})

export class AppComponent {
  title = 'demoapp';
}
