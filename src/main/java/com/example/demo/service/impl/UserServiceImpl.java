package com.example.demo.service.impl;


import com.example.demo.exception.ErrorMessage;
import com.example.demo.exception.OperationException;
import com.example.demo.service.UserService;
import com.example.demo.utils.DateUtil;
import com.example.demo.vo.UserVO;
import com.example.model.User;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private static final User userDao = new User().dao();

    @Override
    public boolean addUser(UserVO userVo) {
        return new User().set("id", userVo.getId())
                .set("name", userVo.getName())
                .set("age", userVo.getAge())
                .set("character", userVo.getCharacter())
                .set("introduction", userVo.getIntroduction()).save();
    }

    @Override
    public boolean deleteUser(Integer id) {
        return userDao.deleteById(id);
    }

    @Override
    public boolean updateUser(UserVO userVO) {
        return new User().set("id", userVO.getId())
                .set("name", userVO.getName())
                .set("age", userVO.getAge())
                .set("character", userVO.getCharacter())
                .set("introduction", userVO.getIntroduction()).update();
    }

    @Override
    public User findUserById(Integer id) {

        String sqlSearch = "SELECT * FROM USER WHERE id = ?";
        return (User) new User().dao().findFirst(sqlSearch,id);
    }

}
