package com.example.demo.service;

import com.example.demo.vo.UserVO;
import com.example.model.User;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;

public interface UserService {

    /**
     *  增
     * */
    boolean addUser(UserVO userVo);
    /**
     * 删
     * */
    boolean deleteUser(Integer id);

    /**
     * 改
     * */
    boolean updateUser(UserVO userVO);

    /**
     * 查
     * */
    User findUserById(Integer id);

}
