package com.example.demo.config;

import com.alibaba.druid.wall.WallFilter;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.druid.DruidPlugin;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.sql.Connection;

@Configuration
public class JfinalDemoConfig {

    @Value("${spring.datasource.url}")
    private String url;

    @Value("${spring.datasource.username}")
    private String username;

    @Value("${spring.datasource.password}")
    private String password;

    @Bean
    public ActiveRecordPlugin initActiveRecordPlugin(){
        DruidPlugin dp=new DruidPlugin(url,username,password);
        //加强数据库操作
        WallFilter wallFilter=new WallFilter();
        wallFilter.setDbType("mysql");
        dp.addFilter(wallFilter);
        // 必须手动调用start
        dp.start();

        ActiveRecordPlugin arp=new ActiveRecordPlugin(dp);
        //设置事务级别
        arp.setTransactionLevel(Connection.TRANSACTION_READ_COMMITTED);
        //添加model到数据库表的映射
        com.example.model._MappingKit.mapping(arp);
        //设置是否显示sql
        arp.setShowSql(true);
        arp.getEngine().setToClassPathSourceFactory();
       /* //添加模板
        arp.addSqlTemplate("/sql/all_sqls.sql");*/
        // 必须手动调用start
        arp.start();

//        log.info("ActiveRecordPlugin初始化完成");
        return arp;
    }


}
