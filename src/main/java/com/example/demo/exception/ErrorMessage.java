package com.example.demo.exception;

/**
 * @Author: wutongshu
 * @Description:
 * @Date: Created in 18:03 2020/5/14
 * @Modified By:
 */
public class ErrorMessage {
    private String errorMessage;
    private String errorCode;

    public ErrorMessage(String errorMessage,String errorCode){
        this.errorMessage=errorMessage;
        this.errorCode=errorCode;
    }

    public ErrorMessage(String errorMessage){
        this.errorMessage=errorMessage;
    }

    public String getErrorMessage(){
        return errorMessage;
    };

    public String getErrorCode(){
        return errorCode;
    };
}
