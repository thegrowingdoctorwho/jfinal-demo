package com.example.demo.exception;

import lombok.Getter;
import lombok.Setter;

/**
 * @Author: wutongshu
 * @Description:
 * @Date: Created in 18:00 2020/5/14
 * @Modified By:
 */
public class OperationException extends RuntimeException {


    @Setter
    @Getter
    private ErrorMessage errorMessage;

    @Setter
    @Getter
    private Object attachInfo;

    public OperationException(ErrorMessage errorMessage, Throwable cause, Object attachInfo) {
        super(errorMessage.getErrorMessage(), cause);
        this.errorMessage = errorMessage;
        this.attachInfo = attachInfo;
    }


    public OperationException(ErrorMessage errorMessage) {
        this(errorMessage, null, null);
    }

    public OperationException(ErrorMessage errorMessage, Throwable cause) {
        this(errorMessage, cause, null);
    }

    public OperationException(ErrorMessage errorMessage, Object attachInfo) {
        this(errorMessage, null, attachInfo);
    }

}