package com.example.demo.utils;


import com.alibaba.druid.util.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @Author: wutongshu
 * @Description:日期格式化
 * @Date: Created in 14:29 2020/5/13
 * @Modified By:
 */
public class DateUtil {
    private final static String defaultDatePattern = "yyyy-MM-dd";

    /**
     * 格式化Date成字符串
     */
    public static String formatDate(Date date) {
        return date == null ? "" : new SimpleDateFormat(defaultDatePattern).format(date);
    }

    /**
     *  将字符串转为Date
     */
    public static Date parseDate(String strDate) {
        try {
            return StringUtils.isEmpty(strDate) ? null : new SimpleDateFormat(defaultDatePattern).parse(strDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }
}
