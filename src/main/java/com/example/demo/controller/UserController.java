package com.example.demo.controller;

import com.example.demo.service.UserService;
import com.example.demo.vo.UserVO;
import com.example.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    UserService userService;

    /**
     * 增
     * */
    @PostMapping(value = "/add")
    public boolean addUser(@RequestBody UserVO userVo){
        return userService.addUser(userVo);
    }



    /**
     * 删
     * */
    @DeleteMapping("/del")
    public boolean deleteUser(@RequestBody UserVO userVo){
        return userService.deleteUser(userVo.getId());
    }

    /**
     * 查
     * */
    @GetMapping("/find")
    public User findUser(@RequestParam Integer id){
        return userService.findUserById(id);
    }


    /**
     * 改
     * */
    @PutMapping("update")
    public boolean updateUser(@RequestBody UserVO userVo){
        return userService.updateUser(userVo);
    }



}
