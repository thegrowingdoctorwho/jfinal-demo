### ReadMe:



注意：

后端为spring boot+ jfinal,本地运行直接在idea中构建启动即可。



前端为angular10 构建的项目，代码位置 src/main/webapp/中，如果需要在本地运行需要安装相关环境。



```
前端相关安装：
1、 node.js
2、 angular 10

node.js 安装： https://nodejs.org/zh-cn/
angular 安装 -> https://angular.cn/guide/setup-local

环境预备完毕，进入到项目目录 /src/main/webapp/ 下，
1、运行npm install
2、运行ng serve 或npm start


```





注意事项

```
前端访问地址： localhost:4200 （默认）
如需更改前端项目默认端口可在启动时 使用命令： ng serve --port 5000 ，更改端口

后端地址为：localhost:8080

```





